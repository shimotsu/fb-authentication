import firebase from 'firebase'

if (!firebase.apps.length) {
  firebase.initializeApp({
    apiKey: 'AIzaSyDXzIZTAAF6_AfdBipN9i7pqcudRB_ZTwg',
    authDomain: 'fb-authentication-dcac6.firebaseapp.com',
    databaseURL: 'https://fb-authentication-dcac6.firebaseio.com',
    projectId: 'fb-authentication-dcac6',
    storageBucket: 'fb-authentication-dcac6.appspot.com',
    messagingSenderId: '215272613565'
  })
}

export default firebase
